#!/bin/bash
if (( $EUID != 0 )); then
    echo "Please run as root."
    exit
fi
mkdir /usr/share/tex
cp -r templates /usr/share/tex
echo "Successfully installed."
